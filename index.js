function Pokemon(name, lvl, hp, atk, crit){
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.attack = atk;
	this.critical = crit;


	this.tackle = function(target){
		console.log(this.name + ' used tackle on ' + target.name);
		let newHP = target.health -= this.attack;
		console.log(target.name + "'s hp" + ' is now ' + newHP);
		if (newHP < 10) {
		console.log(target.name + ' has fainted')
		};
	}

	this.flamethrower = function(target){
		console.log(this.name + ' used flamethrower on ' + target.name);
		newHP = target.health -= this.critical;
		console.log(target.name + "'s hp" + ' is now ' + newHP);
		if (newHP < 10) {
		console.log(target.name + ' has fainted')
		};
	}

	this.skillMiss = function(target){
		console.log(this.name + ' used tackle on ' + target.name);
		console.log(target.name + "'s hp" + ' is still ' + target.health + ' because the attack missed.')
		};
	}


// creating instance

let charizard = new Pokemon('Charizard', 90, 1000, 100, 500);
let bulbasaur = new Pokemon('Bulbasaur', 90, 800, 20, 100);

charizard.tackle(bulbasaur)
bulbasaur.tackle(charizard)
charizard.tackle(bulbasaur)
bulbasaur.skillMiss(charizard)
charizard.tackle(bulbasaur)
charizard.flamethrower(bulbasaur)